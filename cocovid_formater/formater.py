import json
import os


class Formater:
    def __init__(self, name_output):
        self.categories = []
        self.annotations = []
        self.images = []
        self.videos = []
        self.cocovid_file = {"categories": [],
                             "annotations": [],
                             "images": [],
                             "videos": []}
        self.fn_to_videoId = {}
        self.path_to_save = os.path.join(os.path.dirname(__file__), "..", "ressources", "output", name_output)
        self.path_coco_dopi = os.path.join(os.path.dirname(__file__), "..", "ressources", "input", name_output)

    def run(self) -> None:
        self.spawn_cat()
        self.spawn_videos(self.path_coco_dopi)
        self.spawn_annotations(path_to_coco=self.path_coco_dopi)
        self.spawn_images(path_to_coco=self.path_coco_dopi)
        self.cocovid_file["categories"] = self.categories
        self.cocovid_file["annotations"] = self.annotations
        self.cocovid_file["images"] = self.images
        self.cocovid_file["videos"] = self.videos

        with open(self.path_to_save, "w") as f:
            json.dump(self.cocovid_file, f)

    def filename_parse(self, filename):
        uniq_name = filename[:-11]
        frame_number = filename[-10:-7]
        return uniq_name, frame_number

    def instantiate_fn_to_video_mapper(self, path_to_coco):
        mapper_brut_to_fn = {}
        mapper_fn_to_videoId = {}
        with open(path_to_coco, 'r') as f:
            json_file = json.load(f)
        imgs_annotations = json_file["images"]
        filenames = []
        for img_annotation in imgs_annotations:
            uniq_name, frame_number = self.filename_parse(img_annotation["file_name"])
            filenames.append(uniq_name)
            mapper_brut_to_fn[img_annotation["file_name"]] = uniq_name
        filenames = set(filenames)
        uniq_video_id = 1
        for fn in filenames:
            mapper_fn_to_videoId[fn] = uniq_video_id
            uniq_video_id +=1
        return mapper_brut_to_fn, mapper_fn_to_videoId

    def spawn_cat(self) -> None:
        cat_0 = {
            "id": 0,
            "name": "0"
        }

        cat_1 = {"id": 1,
                 "name": "1"
                 }

        cat_2 = {"id": 2,
                 "name": "2"
                 }

        cat_3 = {"id": 3,
                 "name": "3"
                 }

        cat_4 = {"id": 4,
                 "name": "4"
                 }

        cat_5 = {"id": 5,
                 "name": "5"
                 }

        cat_6 = {"id": 6,
                 "name": "6"
                 }

        cat_7 = {"id": 7,
                 "name": "7"
                 }

        self.categories.extend((cat_0, cat_1, cat_2, cat_3, cat_4, cat_5, cat_6, cat_7))

    def spawn_videos(self, path_to_coco) -> None:
        with open(path_to_coco, 'r') as f:
            json_file = json.load(f)
        imgs_annotations = json_file["images"]
        mapper_brut_to_fn, mapper_fn_to_videoId = self.instantiate_fn_to_video_mapper(path_to_coco)

        for img_annotation in imgs_annotations:
            correct_fn = str(mapper_brut_to_fn[img_annotation['file_name']])
            tmp = {"id": mapper_fn_to_videoId[correct_fn],
                   "name": correct_fn}

            if tmp not in self.videos:
                self.videos.append(tmp)

    def spawn_images(self, path_to_coco):
        with open(path_to_coco, "r") as f:
            json_file = json.load(f)
        imgs_annotations = json_file["images"]
        mapper_brut_to_fn, mapper_fn_to_videoId = self.instantiate_fn_to_video_mapper(path_to_coco)
        for img_annotation in imgs_annotations:
            correct_fn = str(mapper_brut_to_fn[img_annotation['file_name']])
            self.images.append({
                "file_name": correct_fn,
                "height": int(img_annotation["height"]),
                "width": int(img_annotation["width"]),
                "id": (int(img_annotation["id"]) + 1),
                "video_id": mapper_fn_to_videoId[correct_fn],
                "frame_id": (int(img_annotation["id"]))
            })

    def spawn_annotations(self, path_to_coco) -> None:
        with open(path_to_coco, 'r') as f:
            json_file = json.load(f)
        imgs_annotations = json_file["annotations"]
        for img_annotation in imgs_annotations:
            del img_annotation["segmentation"]
            self.annotations.append(img_annotation)


formater = Formater("test_local.json")
formater.run()
