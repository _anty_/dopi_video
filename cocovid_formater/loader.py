import os
import shutil


class Loader:
    def __init__(self):
        self.path_annotations = os.path.join(os.path.dirname(__file__),
                                             "..", "ressources", "annotations")
        self.path_frames = os.path.join(os.path.dirname(__file__),
                                        "..", "ressources", "annotations","frames")
        self.path_boxes = os.path.join(os.path.dirname(__file__),
                                        "..", "ressources", "annotations","boxes")
        self.path_dataset = os.path.join(os.path.dirname(__file__),
                                         "..", "ressources", "dataset")

    def move_file(self, src, dst):
        shutil.move(src, dst)

    def train_test_split(self) -> None:
        list_frames = os.listdir(
            os.path.join(self.path_annotations, "frames")
        )
        list_frames = [frame for frame in list_frames if "Copy of" not in frame]
        list_annotations = [(frame[:-3] + "txt") for frame in list_frames]

        frames_and_annotations = list(zip(list_frames, list_annotations))

        app_size = int(0.5* len(list_frames))
        val_size = int(0.7* len(list_frames))
        app = frames_and_annotations[:app_size]
        val = frames_and_annotations[app_size: val_size]
        test = frames_and_annotations[val_size:]

        # FIXME : function for this garbage pls :)
        for frames_path, annotations_path in app:
            shutil.copy(os.path.join(self.path_frames, frames_path),
                        os.path.join(self.path_dataset, "app","frames", frames_path))
            shutil.copy(os.path.join(self.path_boxes, annotations_path),
                        os.path.join(self.path_dataset, "app", "boxes", annotations_path))

        for frames_path, annotations_path in val:
            shutil.copy(os.path.join(self.path_frames, frames_path),
                        os.path.join(self.path_dataset, "val", "frames", frames_path))
            shutil.copy(os.path.join(self.path_boxes, annotations_path),
                        os.path.join(self.path_dataset, "val", "boxes", annotations_path))

        for frames_path, annotations_path in test:
            shutil.copy(os.path.join(self.path_frames, frames_path),
                        os.path.join(self.path_dataset, "test", "frames", frames_path))
            shutil.copy(os.path.join(self.path_boxes, annotations_path),
                        os.path.join(self.path_dataset, "test","boxes", annotations_path))

loader = Loader()
loader.train_test_split()