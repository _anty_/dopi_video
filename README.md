# dopi_video
#### adresse dns machine distante 15.188.27.93
#### __________REMPLACER LES CHEMINS!!!__________

#### yolo to coco
* python main.py --path C:\Users\33758\PycharmProjects\dopi_video_object_recognition\ressources\dataset\app\ --output app
* python main.py --path C:\Users\33758\PycharmProjects\dopi_video_object_recognition\ressources\dataset\val\ --output val
* python main.py --path C:\Users\33758\PycharmProjects\dopi_video_object_recognition\ressources\dataset\test\ --output test

# coco to cocovid
open formater.py class and run it

# setup mmtrack requirements

* curl -o /home/admin/Anaconda3-2020.11-Linux-x86_64.sh https://repo.anaconda.com/archive/Anaconda3-2020.11-Linux-x86_64.sh
* bash Anaconda-latest-Linux-x86_64.sh

* conda create -n open-mmlab python=3.7 -y
* conda activate open-mmlab

* conda install pytorch==1.6.0 torchvision==0.7.0 cudatoolkit=10.1 -c pytorch -y

# install the latest mmcv
pip install mmcv-full -f https://download.openmmlab.com/mmcv/dist/cu101/torch1.6.0/index.html

# install mmdetection
pip install mmdet

# install mmtracking
* git clone https://github.com/open-mmlab/mmtracking.git
* cd mmtracking
* pip install -r requirements/build.txt
* pip install -v -e .

# Drivers Cuda
* wget https://us.download.nvidia.com/XFree86/linux-x86_64/460.32.03/NVIDIA-Linux-x86_64-460.32.03.run
* sudo bash NVIDIA-Linux-x86_64-460.32.03.run
* nvidia-smi

# Les modeles 
* SELSA : https://arxiv.org/pdf/1907.06390.pdf
* DFF : https://arxiv.org/pdf/1611.07715.pdf

# Train
python tools/train.py "/home/admin/dopi_video/mmtracking_/configs/_base_/datasets/custom_vid_fgfa.py"

# Test
python tools/test.py  "C:\Users\33758\PycharmProjects\dopi_video_object_recognition\mmtracking\configs\_base_\datasets\custom_vid_fgfa.py" --checkpoint "C:\Users\33758\PycharmProjects\dopi_video_object_recognition\dopi_video\epoch_2.pth"

# metriques :
python tools/analyze_logs.py plot_curve ../mmtrack_output/tmp/20210506_131026.log.json --keys loss_cls loss_bbox

