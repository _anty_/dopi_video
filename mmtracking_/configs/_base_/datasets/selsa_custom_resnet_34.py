_base_ = [
    '../../_base_/models/faster_rcnn_r50_dc5.py'
]
model = dict(
    type='SELSA',
    pretrains=None,
    detector=dict(
        pretrained='torchvision://resnet18',
        backbone=dict(depth=18),
        roi_head=dict(
            type='SelsaRoIHead',
            bbox_head=dict(
                type='SelsaBBoxHead',
                num_shared_fcs=2,
                aggregator=dict(
                    type='SelsaAggregator',
                    in_channels=1024,
                    num_attention_blocks=16)))))

# dataset settings
dataset_type = 'CocoVideoDataset'
classes= ('0','1','2','3','4','5','6','7')
#data_root = "C:/Users/33758/PycharmProjects/dopi_video_object_recognition/ressources"
data_root = "/home/admin/dopi_video/ressources"
img_norm_cfg = dict(
    mean=[123.675, 116.28, 103.53], std=[58.395, 57.12, 57.375], to_rgb=True)
train_pipeline = [
    dict(type='LoadMultiImagesFromFile'),
    dict(type='SeqLoadAnnotations', with_bbox=True, with_track=True),
    dict(type='SeqResize', img_scale=(1000, 600), keep_ratio=True),
    dict(type='SeqRandomFlip', share_params=True, flip_ratio=0.5),
    dict(type='SeqNormalize', mean=[123.675, 116.28, 103.53], std=[58.395,57.12,57.375]),
    dict(type='SeqPad', size_divisor=16),
    dict(
        type='VideoCollect',
        keys=['img', 'gt_bboxes', 'gt_labels', 'gt_instance_ids']),
    dict(type='ConcatVideoReferences'),
    dict(type='SeqDefaultFormatBundle', ref_prefix='ref')
]
test_pipeline = [
    dict(type='LoadMultiImagesFromFile'),
    dict(type='SeqResize', img_scale=(1000, 600), keep_ratio=True),
    dict(type='SeqRandomFlip', share_params=True, flip_ratio=0.0),
    dict(type='SeqNormalize', **img_norm_cfg),
    dict(type='SeqPad', size_divisor=16),
    dict(
        type='VideoCollect',
        keys=['img'],
        meta_keys=('num_left_ref_imgs', 'frame_stride')),
    dict(type='ConcatVideoReferences'),
    dict(type='MultiImagesToTensor', ref_prefix='ref'),
    dict(type='ToList')
]
data = dict(
    samples_per_gpu=1,
    workers_per_gpu=2,
    train=[
        dict(
            type=dataset_type,
            classes=classes,
            ann_file=data_root +"/output/app.json",
            img_prefix=data_root + "/dataset/app/frames",
            ref_img_sampler=dict(
                num_ref_imgs=2,
                frame_range=9,
                filter_key_img=True,
                method='bilateral_uniform'),
            pipeline=train_pipeline)
    ],
    val=dict(
        type=dataset_type,
        classes=classes,
        ann_file=data_root +"/output/val.json",
        img_prefix=data_root +"/dataset/val/frames",
        ref_img_sampler=dict(
            num_ref_imgs=30,
            frame_range=[-15, 15],
            stride=1,
            method='test_with_fix_stride'),
        pipeline=test_pipeline,
        test_mode=True),
    test=dict(
        type=dataset_type,
        ann_file=data_root+"/output/test_local.json",
        classes=classes,
        img_prefix=data_root +"/dataset/test_local/frames",
        ref_img_sampler=dict(
            num_ref_imgs=30,
            frame_range=[-15, 15],
            stride=1,
            method='test_with_fix_stride'),
        pipeline=test_pipeline,
        test_mode=True))

optimizer = dict(type='Adam', lr=0.0001, weight_decay=0.0001)  # Config used to build optimizer, support all the optimizers in PyTorch whose arguments are also the same as those in PyTorch
optimizer_config = dict(grad_clip=dict(max_norm=35, norm_type=2))  # Config used to build the optimizer hook, refer to https://github.com/open-mmlab/mmcv/blob/master/mmcv/runner/hooks/optimizer.py#L8 for implementation details.
checkpoint_config = dict(interval=1)  # Config to set the checkpoint hook, Refer to https://github.com/open-mmlab/mmcv/blob/master/mmcv/runner/hooks/checkpoint.py for implementation.
log_config = dict(interval=50, hooks=[dict(type='TextLoggerHook')])  # config to register logger hook
dist_params = dict(backend='nccl', port='29500') # Parameters to setup distributed training, the port is set to 29500 by default
log_level = 'INFO'  # The level of logging.
load_from = None  # load models as a pre-trained model from a given path. This will not resume training.
resume_from = None  # Resume checkpoints from a given path, the training will be resumed from the epoch when the checkpoint's is saved.
workflow = [('train', 1)]  # Workflow for runner. [('train', 1)] means there is only one workflow and the workflow named 'train' is executed once. The workflow trains the model by 7 epochs according to the total_epochs.
lr_config = dict(  # Learning rate scheduler config used to register LrUpdater hook
    policy='step',
    warmup='linear',
    warmup_iters=500,
    warmup_ratio=0.3333333333333333,
    step=[2, 5])
total_epochs = 2  # Total epochs to train the model
evaluation = dict(metric=['bbox'], interval=2)  # The config to build the evaluation hook
work_dir = '../mmtrack_output/tmp_selsa'  # Directory to save the model checkpoints and logs for the current experiments.
