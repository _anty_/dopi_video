import os
import shutil


class Explorer:
    def __init__(self):
        self.threshold = 0.7
        self.path_to_folder = os.path.join(os.path.dirname(__file__), "..", "ressources", "dopi_dataset_final", "dopi_dataset_final")
        self.path_to_boxes = os.path.join(os.path.dirname(__file__), "..", "ressources", "annotations", "boxes")
        self.path_to_frames = os.path.join(os.path.dirname(__file__), "..", "ressources", "annotations", "frames")

    def get_extension(self, path):
        filename, extension = os.path.splitext(path)
        return extension

    def seperate_boxes_from_frames(self):
        for path_to_file in os.listdir(self.path_to_folder):
            extension = self.get_extension(path_to_file)
            if extension == ".txt":
                shutil.move(
                    os.path.join(self.path_to_folder, path_to_file), os.path.join(self.path_to_boxes, path_to_file) )

            if extension == ".jpg":
                shutil.move(
                    os.path.join(self.path_to_folder, path_to_file), os.path.join(self.path_to_frames, path_to_file))


    def load_annotations(self):
        pass
loader = Explorer()
loader.seperate_boxes_from_frames()